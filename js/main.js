$('.single-item').slick({
    autoplay: true,
    autoplaySpeed: 1000,
    speed:2000,
    arrows:false,
    fade:true,
    dots:true
});
$('.reviews-list').slick({
    autoplay: true,
    autoplaySpeed: 1000,
    speed:1000,
    arrows:false,
    dots:true,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive:[
        {
            breakpoint: 1200,
            settings: {
                slidesToShow: 1,
            }
        },
    ]
});